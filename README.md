# Krafi

A long time ago in a beach, far far away...<br>
NMA geeks joked that they should make Trafi, but better - Krafi (no offence).

And thus, this project was born.<br>
Oh yeah, I also missed the part how it's actually an internship task.

## Previews
Successful runs (and many more actually, it works surprisingly well for such straight-forward approach!) 
![sauletekis-silenu.gif](https://gitlab.com/Sawas/krafi/raw/master/Previews/sauletekis-silenu.gif)
![](https://gitlab.com/Sawas/krafi/raw/master/Previews/studentu-griunvaldo.gif) <br>
Of course it's still far away from even being usable (there is almost a direct transit for this one...)
![](https://gitlab.com/Sawas/krafi/raw/master/Previews/medeksine-nuokalnes.gif)  